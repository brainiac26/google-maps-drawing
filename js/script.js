(function () {
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var map,
            drawingManager,
            overlays = [],
            marker,
            place,
            selectedShape,
            autocomplete,

            addressSearchInput = document.getElementById('address-search'),
            streetInput = document.getElementById('street'),
            cityInput = document.getElementById('city'),
            stateInput = document.getElementById('state'),
            countryInput = document.getElementById('country'),
            postalCodeInput = document.getElementById('postal-code'),
            sqft = document.getElementById('sqft'),
            totalSqft = document.getElementById('total-sqft'),
            deleteShapeButton = document.getElementById('delete-shapes');

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(36.2398864, -113.7644643),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            rotateControl: false,
            streetViewControl: false
        });

        map.setTilt(0); // turns off angled view

        autocomplete = new google.maps.places.Autocomplete(addressSearchInput);

        autocomplete.addListener('place_changed', function () {
            var placeDetails;

            place = autocomplete.getPlace();

            placeDetails = getPlaceDetails(place);
            streetInput.value = placeDetails.streetNumber + ' ' + placeDetails.streetName;
            cityInput.value = placeDetails.city;
            stateInput.value = placeDetails.state;
            countryInput.value = placeDetails.country;
            postalCodeInput.value = placeDetails.postalCode;

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map
                });
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        });

        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.CIRCLE,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.CIRCLE,
                    google.maps.drawing.OverlayType.POLYGON,
                    google.maps.drawing.OverlayType.RECTANGLE
                ]
            },
            polygonOptions: {
                fillColor: '#67689B',
                strokeColor: '#595989',
                fillOpacity: 0.3,
                strokeWeight: 1,
                clickable: true,
                editable: true,
                draggable: true
            },
            rectangleOptions: {
                fillColor: '#67689B',
                strokeColor: '#595989',
                fillOpacity: 0.3,
                strokeWeight: 1,
                clickable: true,
                editable: true,
                draggable: true
            },
            circleOptions: {
                fillColor: '#67689B',
                strokeColor: '#595989',
                fillOpacity: 0.3,
                strokeWeight: 1,
                clickable: true,
                editable: true,
                zIndex: 1
            }
        });

        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            var newShape = e.overlay;
            overlays.push(e);
            google.maps.event.addListener(newShape, 'click', function () {
                setSelection(newShape);
            });
            setSelection(newShape);
            displaySqft();
        });

        // Circle
        google.maps.event.addListener(drawingManager, 'circlecomplete', function (circle) {
            google.maps.event.addListener(circle, 'radius_changed', function () {
                displaySqft();
            });
        });

        // Polygon
        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            var polygonPath = polygon.getPath();
            google.maps.event.addListener(polygon, 'dragend', function () {
                displaySqft();
            });

            google.maps.event.addListener(polygonPath, 'set_at', function () {
                displaySqft();
            });

            google.maps.event.addListener(polygonPath, 'insert_at', function () {
                displaySqft();
            });
        });

        // Rectangle
        google.maps.event.addListener(drawingManager, 'rectanglecomplete', function (rectangle) {
            google.maps.event.addListener(rectangle, 'bounds_changed', function () {
                displaySqft();
            });
        });

        deleteShapeButton.addEventListener('click', function () {
            deleteSelectedShape();
        });

        function clearSelection() {
            if (selectedShape) {
                selectedShape.setEditable(false);
                selectedShape = null;
            }
        }

        function setSelection(shape) {
            clearSelection();
            selectedShape = shape;
            shape.setEditable(true);
        }

        function deleteSelectedShape() {
            if (selectedShape) {
                overlays.splice(findOverlayIndex(selectedShape), 1);
                selectedShape.setMap(null);
                selectedShape = null;
                displaySqft();
            }
        }

        function findOverlayIndex(shape) {
            var index;
            for (var i = 0, l = overlays.length; i < l; i++) {
                if (shape === overlays[i].overlay) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        function getPlaceDetails(place) {
            var streetFieldName = 'route',
                streetFieldNumber = 'street_number',
                cityFieldName = 'locality',
                stateFieldName = 'administrative_area_level_1',
                countryFieldName = 'country',
                postalCodeFieldName = 'postal_code';

            placeDetails = {
                streetName: '',
                streetNumber: '',
                city: '',
                state: '',
                country: '',
                postalCode: ''
            };
            if (place.address_components.length) {
                for (var i = 0, l = place.address_components.length; i < l; i++) {
                    var addressComponent = place.address_components[i];
                    if (addressComponent.types.indexOf(streetFieldNumber) !== -1) {
                        placeDetails.streetNumber = addressComponent.long_name;
                    }
                    if (addressComponent.types.indexOf(streetFieldName) !== -1) {
                        placeDetails.streetName = addressComponent.long_name;
                    }
                    if (addressComponent.types.indexOf(cityFieldName) !== -1) {
                        placeDetails.city = addressComponent.long_name;
                    }
                    if (addressComponent.types.indexOf(stateFieldName) !== -1) {
                        placeDetails.state = addressComponent.long_name;
                    }
                    if (addressComponent.types.indexOf(countryFieldName) !== -1) {
                        placeDetails.country = addressComponent.long_name;
                    }
                    if (addressComponent.types.indexOf(postalCodeFieldName) !== -1) {
                        placeDetails.postalCode = addressComponent.long_name;
                    }
                }
            }
            return placeDetails;
        }

        function displaySqft() {
            var areas = [],
                totalArea = 0;

            for (var i = 0, l = overlays.length; i < l; i++) {
                var overlay = overlays[i],
                    area;
                switch (overlay.type) {
                    case 'circle':
                        area = getCircleArea(overlay);
                        break;
                    case 'rectangle':
                        area = getRectangleArea(overlay);
                        break;
                    case 'polygon':
                        area = getPolygonArea(overlay);
                        break;
                }
                totalArea += area;
                areas.push(area);
            }
            sqft.innerHTML = areas.join(",  ");
            totalSqft.innerHTML = totalArea;
        }

        function getCircleArea(circle) {
            var radius = circle.overlay.radius;
            return sqmToSqft(radius * radius * Math.PI);
        }

        function getRectangleArea(rectangle) {
            var bounds = rectangle.overlay.bounds;
            var sw = bounds.getSouthWest(),
                ne = bounds.getNorthEast(),
                southWest = new google.maps.LatLng(sw.lat(), sw.lng()),
                northEast = new google.maps.LatLng(ne.lat(), ne.lng()),
                southEast = new google.maps.LatLng(sw.lat(), ne.lng()),
                northWest = new google.maps.LatLng(ne.lat(), sw.lng());
            return sqmToSqft(google.maps.geometry.spherical.computeArea([northEast, northWest, southWest, southEast]));
        }

        function getPolygonArea(polygon) {
            return sqmToSqft(google.maps.geometry.spherical.computeArea(polygon.overlay.getPath()));
        }

        function sqmToSqft(sqm) {
            return sqm * 10.764;
        }
    }
})();